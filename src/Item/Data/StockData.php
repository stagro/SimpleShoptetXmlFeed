<?php

namespace SimpleShoptetXmlFeed\Item\Data;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class StockData
 * @package SimpleShoptetXmlFeed\Item\Data
 */
class StockData
{
    /**
     * @var int
     * @Serializer\Type("int")
     * @Serializer\SerializedName("AMOUNT")
     */
    private $amount;

    /**
     * @var int
     * @Serializer\Type("int")
     * @Serializer\SerializedName("MINIMAL_AMOUNT")
     * @Serializer\SkipWhenEmpty()
     */
    private $minimalAmount;

    /**
     * @param int $amount
     */
    public function setAmount(int $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $minimalAmount
     */
    public function setMinimalAmount(int $minimalAmount)
    {
        $this->minimalAmount = $minimalAmount;
    }

    /**
     * @return int
     */
    public function getMinimalAmount(): int
    {
        return $this->minimalAmount;
    }
}
