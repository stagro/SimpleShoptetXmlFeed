<?php

namespace SimpleShoptetXmlFeed\Item\Data;

use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class AlternativeProductData
 * @package SimpleShoptetXmlFeed\Item\Data
 */
class AlternativeProductData
{
    /**
     * @var ArrayCollection
     * @Serializer\XmlList(inline=true, entry="CODE")
     */
    private $alternativeProducts;

    /**
     * AlternativeProductData constructor.
     */
    public function __construct()
    {
        $this->alternativeProducts = new ArrayCollection();
    }

    /**
     * @param string $product
     */
    public function addAlternativeProduct(string $product)
    {
        if (!$this->alternativeProducts->contains($product)) {
            $this->alternativeProducts->add($product);
        }
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getAlternativeProducts(): ArrayCollection
    {
        return $this->alternativeProducts;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $alternativeProducts
     */
    public function setAlternativeProducts(ArrayCollection $alternativeProducts)
    {
        $this->alternativeProducts = $alternativeProducts;
    }
}
