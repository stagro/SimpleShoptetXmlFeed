<?php

namespace SimpleShoptetXmlFeed\Item\Data;

use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class ImageData
 * @package SimpleShoptetXmlFeed\Item\Data
 */
class ImageData
{
    /**
     * @var ArrayCollection
     * @Serializer\XmlList(inline=true, entry="IMAGE")
     */
    private $images;

    /**
     * ImageData constructor.
     */
    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * @param string $image
     */
    public function addImage(string $image)
    {
        if (!$this->images->contains($image)) {
            $this->images->add($image);
        }
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getImages(): ArrayCollection
    {
        return $this->images;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $images
     */
    public function setImages(ArrayCollection $images)
    {
        $this->images = $images;
    }
}
