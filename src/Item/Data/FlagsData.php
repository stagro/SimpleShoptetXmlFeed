<?php

namespace SimpleShoptetXmlFeed\Item\Data;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class FlagsData
 * @package SimpleShoptetXmlFeed\Item\Data
 */
class FlagsData
{
    /**
     * @var int
     * @Serializer\Type("int")
     * @Serializer\SerializedName("ACTION")
     * @Serializer\SkipWhenEmpty()
     */
    private $action;

    /**
     * @var int
     * @Serializer\Type("int")
     * @Serializer\SerializedName("NEW")
     * @Serializer\SkipWhenEmpty()
     */
    private $new;

    /**
     * @var int
     * @Serializer\Type("int")
     * @Serializer\SerializedName("TIP")
     * @Serializer\SkipWhenEmpty()
     */
    private $tip;

    /**
     * @param bool $action
     */
    public function setAction(bool $action)
    {
        $this->action = (int)$action;
    }

    /**
     * @return int
     */
    public function getAction(): int
    {
        return $this->action;
    }

    /**
     * @param bool $new
     */
    public function setNew(bool $new)
    {
        $this->new = (int)$new;
    }

    /**
     * @return int
     */
    public function getNew(): int
    {
        return $this->new;
    }

    /**
     * @param bool $tip
     */
    public function setTip(bool $tip)
    {
        $this->tip = (int)$tip;
    }

    /**
     * @return int
     */
    public function getTip(): int
    {
        return $this->tip;
    }
}
